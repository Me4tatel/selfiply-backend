const BlackList = require('./models/blacklist.js')

const jwt = require('jsonwebtoken');

const secret = 'likeme4';

const withAuth = function(req, res, next) {
    const token =
        req.body.token ||
        req.query.token ||
        req.headers['x-access-token'] ||
        req.cookies.token;

    if (!token) {
        res.status(401).send('Unauthorized: No token provided');
    } else {
        BlackList.findOne({ 'token': token }, 'token', function(err, tokens) {
            if (tokens) {
                res.status(402).send('Unauthorized: Invalid token');
            } else {
                jwt.verify(token, secret, function(err, decoded) {
                    if (err) {
                        res.status(402).send('Unauthorized: Invalid token');
                    } else {
                        req.email = decoded.email;
                        const token2 = jwt.sign({ email: req.email }, secret, {
                            expiresIn: '2m'
                        });
                        res.cookie('token', token2, { httpOnly: true });
                        next();
                    }
                });
            }
        });

    }
}

module.exports = withAuth;