const express = require('express');
const withAuth = require('../middleware');
const { getConcern, createNewConcern, updateConcern, updateOrderConcern, deleteConcern } = require('../controllers/concern');

const router = express.Router();

router.route('')
	.get(getConcern)
	.post(withAuth, createNewConcern);

router.route('/:id')
	.get(getConcern)
	.put(withAuth, updateConcern)
	.delete(withAuth, deleteConcern)

router.route('/api/Concerns/UpdateOrder')
	.put(withAuth, updateOrderConcern)

module.exports = router;