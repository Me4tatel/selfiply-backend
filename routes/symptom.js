const express = require('express');
const withAuth = require('../middleware');
const { getSymptom, createNewSymptom, updateSymptom, deleteSymptom } = require('../controllers/symptom');

const router = express.Router();

router.route('')
	.get(getSymptom)
	.post(withAuth, createNewSymptom);

router.route('/:id')
	.get(getSymptom)
	.put(withAuth, updateSymptom)
	.delete(withAuth, deleteSymptom);

module.exports = router;