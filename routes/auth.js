const express = require('express');
const withAuth = require('../middleware');
const { register, login, logout, checkToken } = require('../controllers/auth');


const router = express.Router();

router.get('/home', function(req, res) {
  res.send('Welcome!');
});

router.post('/register', register);

router.post('/authenticate', login);

router.get('/logout', withAuth, logout);

router.get('/checkToken', withAuth, checkToken);

module.exports = router;