const express = require('express');
const withAuth = require('../middleware');
const { getConcernSymptom, addConcernSymptom, updateConcernSymptom, deleteConcernSymptom } = require('../controllers/concernSymptom');

const router = express.Router();

router.route('')
	.put(withAuth, updateConcernSymptom);

router.route('/:id')
	.get(getConcernSymptom)
	.post(withAuth, addConcernSymptom)
	.delete(withAuth, deleteConcernSymptom);

module.exports = router;