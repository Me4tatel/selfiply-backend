const express = require('express');
const withAuth = require('../middleware');
const { getSession, createSession } = require('../controllers/patient.js');

const router = express.Router();

router.route('')
	.get(getSession)
	.post(createSession);

module.exports = router;