var express = require('express');
const jwt = require('jsonwebtoken');

const ConcernSymptom = require('../models/concernSymptom.js');
const Symptom = require('../models/symptom.js');
const Concern = require('../models/concern.js');

const secret = 'likeme4';

const getConcernSymptom = function(req, res) {
    const { id } = req.params;
    let result = {
        data: {
            concernSymptoms: [],
            id: 0,
            name: ""
        },
        error: null,
        isSuccess: true,
        message: "Success"
    }
    if (id) {
        ConcernSymptom.find({ idConcern: id }, { "idConcern":1, "idSymptom":1, "order":1 },  { sort:{ "order":1 }}, function(err, concernSymptoms) {
            Concern.findOne({ _id: id }, { "_id": 1, "name": 1 }, function(err, concern) {
                if(concern){
                    let { data } = result;
                    data.id = concern._id;
                    data.name = concern.name;
                    let arrFunctions = [];
                    concernSymptoms.forEach((item) => {
                        arrFunctions.push(
                            new Promise((resolve, reject) => {
                                Symptom.findOne({ _id: item.idSymptom }, { "_id":1, "text":1, "inputType":1, "placeHolder":1 }, function(err, symptom) {
                                    if(err) reject(err);
                                    resolve({
                                        id: symptom._id,
                                        Order: item.order,
                                        text: symptom.text,
                                        inputType: symptom.inputType,
                                        placeHolder: symptom.placeHolder
                                    });
                                });
                            })
                        )
                    });
                    Promise.all(arrFunctions).then(values => {
                        data.concernSymptoms = values
                        return res.status(200).send(result); 
                    }, reason => {
                        res.status(500).send(reason);
                    });
                } else {
                    res.status(500).send("Error get concernSymptoms");
                }
            });
        });
    } else {
        res.status(500).send("Error get concernSymptoms");
    }
};
const addConcernSymptom = function(req, res) {
    const { id } = req.params;
    const { symptomId } = req.query;
    let result = {
        data: {
            concernSymptoms: [],
            id: 0,
            name: ""
        },
        error: null,
        isSuccess: true,
        message: "Success"
    }
    if (symptomId) {
        let query = Concern.findOne({}, {}, { sort:{ "order":-1 } }, function(err, lastConcern) {
            if (err) {
                console.log(err);
                res.status(500).send("Error in working server.");
            }
            if(lastConcern){
                lastConcern = lastConcern.toObject();
            }
            let order = lastConcern && 
                    lastConcern.hasOwnProperty('order') && 
                    lastConcern.order || 0;
            let concernSymptom = new ConcernSymptom({ idConcern: id, idSymptom: symptomId, order: (order + 1) });
                concernSymptom.save(function(err, concern) {
                    if (err) {
                        console.log(err);
                        res.status(500).send("Error update concernSymptom");
                    }
                    ConcernSymptom.find({ idConcern: id }, { "idConcern":1, "idSymptom":1, "order":1 },  { sort:{ "order":-1 }}, function(err, concernSymptoms) {
                        Concern.findOne({ _id: id }, { "_id": 1, "name": 1 }, function(err, concern) {
                            if(concern){
                                let { data } = result;
                                data.id = concern._id;
                                data.name = concern.name;
                                let arrFunctions = [];
                                concernSymptoms.forEach((item) => {
                                    arrFunctions.push(
                                        new Promise((resolve, reject) => {
                                            Symptom.findOne({ _id: item.idSymptom }, { "_id":1, "text":1, "inputType":1, "placeHolder":1 }, function(err, symptom) {
                                                if(err) reject(err);
                                                resolve({
                                                    id: symptom._id,
                                                    Order: item.order,
                                                    text: symptom.text,
                                                    inputType: symptom.inputType,
                                                    placeHolder: symptom.placeHolder
                                                });
                                            });
                                        })
                                    )
                                });
                                Promise.all(arrFunctions).then(values => {
                                    data.concernSymptoms = values
                                    return res.status(200).send(result); 
                                }, reason => {
                                    res.status(500).send(reason);
                                });
                            } else {
                                res.status(500).send("Error get concernSymptoms");
                            }
                        });
                    });
                })
        });
    } else {
        res.status(500).send("Error added concernSymptom");
    }
};

const updateConcernSymptom = function(req, res) {
    const { concernId, symptomsOrder } = req.body;
    let result = {
        data:{
            concernSymptoms: [],
            id: 61,
            name: "2"
        },
        error: null,
        isSuccess: true,
        message: "Operation completed Successfully"
    }
    let arrUpdates = [];
    symptomsOrder.forEach((item) =>{
        let { symptomId, order } = item;
        arrUpdates.push( new Promise((resolve, reject) => {
            ConcernSymptom.updateOne({ idConcern: concernId, idSymptom: symptomId }, { order:order }, function(err, result){
                if (err) {
                    console.log(err);
                    reject(err);
                } else {
                    Symptom.findOne({ _id: item.symptomId }, { "_id":1, "text":1, "inputType":1, "placeHolder":1 }, function(err, symptom) {
                        if(err) reject(err);
                        resolve({
                            id: symptom._id,
                            Order: item.order,
                            text: symptom.text,
                            inputType: symptom.inputType,
                            placeHolder: symptom.placeHolder
                        });
                    });
                }
            })
        }));
    });

    Promise.all(arrUpdates).then((values) => {
        Concern.findOne({ _id: concernId }, { "_id": 1, "name": 1 }, function(err, concern) {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                let { data } = result;
                data.id = concern._id;
                data.name = concern.name;
                data.concernSymptoms = values;
                res.status(200).send(result);
            };
        });
        
    }, reason => {
        res.status(500).send("Error update concernSymptom" + reason);
    });
};

const deleteConcernSymptom = function(req, res) {
    const { id } = req.params;
    const { symptomId } = req.query;
    ConcernSymptom.deleteOne({ idConcern: id, idSymptom:symptomId }, function(err, element){
        if (err) {
            res.status(500).send("Error delete concern" + err);
        } else {
            let result = {
                data:{
                    concernSymptoms: [],
                    id: 61,
                    name: "2"
                },
                error: null,
                isSuccess: true,
                message: "Operation completed Successfully"
            }
            ConcernSymptom.find({ idConcern: id }, { "idSymptom":1, "order":1 }, function(err, elements){
	    	 	let arrUpdates = [];
			    elements.forEach((item) =>{
			        let { idSymptom, order } = item;
			        arrUpdates.push( new Promise((resolve, reject) => {
	                    Symptom.findOne({ _id: idSymptom }, { "_id":1, "text":1, "inputType":1, "placeHolder":1 }, function(err, symptom) {
	                        if(err) reject(err);
	                        resolve({
	                            id: symptom._id,
	                            Order: order,
	                            text: symptom.text,
	                            inputType: symptom.inputType,
	                            placeHolder: symptom.placeHolder
	                        });
	                    });
			        }));
			    });
			    Promise.all(arrUpdates).then((values) => {
	                Concern.findOne({ _id: id }, { "_id": 1, "name": 1 }, function(err, concern) {
	                    if (err) {
	                        console.log(err);
	                        reject(err);
	                    } else {
	                        let { data } = result;
	                        data.id = concern._id;
	                        data.name = concern.name;
	                        data.concernSymptoms = values;
	                        res.status(200).send(result);
	                    };
	                });
	                
	            }, reason => {
	                res.status(500).send("Error remove concernSymptom" + reason);
	            });
			});
        }
    })
};

module.exports = {
    getConcernSymptom, 
    addConcernSymptom, 
    updateConcernSymptom, 
    deleteConcernSymptom
};