var express = require('express');
const jwt = require('jsonwebtoken');

const Patient = require('../models/patient.js');

const secret = 'likeme4';

const getSession = function(req, res) {
    const { personalNumber } = req.query;
    if (personalNumber) {
        let patient = new Patient({ personalNumber: personalNumber, concernId: 0, patientSymptomsViewModel: []});
        patient.save(function(err, patient) {
            if (err) {
                console.log(err);
                res.status(500).send("Error create session");
            } else {
                res.status(200).send({
                    concernId: 0,
                    patientSymptomsViewModel: [],
                    personalNumber: personalNumber,
                    error: null,
                    isSuccess: true,
                    "message": "your session has created successfully",
                    "data": patient
                });
            }
        });
    } else {
        res.status(500).send({
            "message": "your session has created successfully",
        });
    }
};
const createSession = function(req, res) {
    const data = req.body;
    let answersPatient = Patient.findOne({personalNumber: data.personalNumber}, { "patientSymptomsViewModel":1 }, { sort:{ "_id":-1 }}, function(err, answers){
        console.log(answers);
        if (!answers.patientSymptomsViewModel.length) {
            Patient.updateOne({ personalNumber: data.personalNumber }, { concernId: data.concernId, patientSymptomsViewModel:data.  patientSymptomsViewModel }, function(err, element){
                if (err) {
                    res.status(500).send("Error give answers " + err);
                } else {
                    res.status(200).send(element);
                }
            })
        } else {
            res.status(500).send("Error give answers");
        }
    });
    
};

module.exports = {
    getSession, 
    createSession
};