var express = require('express');
const jwt = require('jsonwebtoken');

const Symptom = require('../models/symptom.js') ;

const secret = 'likeme4';

const getSymptom = function(req, res) {
    const { id } = req;
    if (id) {
        Symptom.findOne({ _id: id }, { "_id":1, "text":1, "inputType":1, "placeHolder":1 }, function(err, symptom) {
            symptom.id = symptom._id;
            delete symptom._id;
            return res.status(200).send(symptom);
        });
    } else {
        Symptom.find({}, {  "_id":1, "text":1, "inputType":1, "placeHolder":1 }, function(err, symptoms) {
            if (err) {
                console.log(err);
                res.status(500).send("Error in working server.");
            }
            symptoms = symptoms.map((item) => {
                let {_id, text, inputType, placeHolder} = item;
                return{
                    id:_id,
                    text: text,
                    inputType: inputType,
                    placeHolder: placeHolder
                }
            })
            let result = {
                "isSuccess": true,
                "error": "string",
                "message": "string",
                "data": symptoms
            }
            return res.status(200).send(result);
        });
    }
};
const createNewSymptom = function(req, res) {
    const { name:text } = req.query;
    const { inputType } = req.query;
    const { placeHolder } = req.query;
    if ( text && inputType ) {
        let symptom = new Symptom({ text: text, inputType: inputType, placeHolder: placeHolder });
        symptom.save(function(err, symptom) {
            if (err) {
                console.log(err);
                res.status(500).send("Error added symptom");
            } else {
                res.status(200).send({
                    id: symptom._id,
                    text: symptom.text,
                    inputType: symptom.inputType,
                    placeHolder: symptom.placeHolder
                });
            }
        })
    } else {
        res.status(500).send("Error added symptom");
    }
};

const updateSymptom = function(req, res) {
    const { name:text } = req.query;
    const { inputType } = req.query;
    const { placeHolder } = req.query;
    const { id } = req.params;
    if ( text && inputType ) {
        Symptom.updateOne({ _id: id }, { text: text, inputType: inputType, placeHolder: placeHolder }, function(err, element){
            if (err) {
                res.status(500).send("Error update symptom" + err);
            } else {
                Symptom.findOne({ _id: id }, { "_id":1, "text":1, "inputType":1, "placeHolder":1 }, function(err, symptom) {
                    if (err) {
                        res.status(500).send("Error update concern" + err);
                    } else {
                        let result = {
                            isSuccess: true, 
                            error: null, 
                            message: "", 
                            data: {
                                id: symptom._id,
                                text: symptom.text,
                                inputType: symptom.inputType,
                                placeHolder: symptom.placeHolder
                            }
                        }
                        res.status(200).send(result);
                    }
                });
            }
        })
    } else {
        res.status(500).send("Error update symptom");
    }
};

const deleteSymptom = function(req, res) {
    const { id } = req;
    Symptom.remove({ _id: id }, function(err, element){
        if (err) {
            res.status(500).send("Error delete symptom" + err);
        } else {
            res.status(200).send({isSuccess:true});
        }
    })
};

module.exports = {
    getSymptom, 
    createNewSymptom, 
    updateSymptom, 
    deleteSymptom
};