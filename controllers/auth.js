var express = require('express');
const jwt = require('jsonwebtoken');

const User = require('../models/user.js');
const BlackList = require('../models/blacklist.js');

const secret = 'likeme4';

const register = function(req, res) {
    const { email, password } = req.body;
    const user = new User({ email, password });
    user.save(function(err) {
        if (err) {
            console.log(err);
            res.status(500).send("Error registering new user please try again.");
        } else {
            res.status(200).send("Welcome to the club!");
        }
    });
};

const login = function(req, res) {
    const { email, password } = req.body;
    User.findOne({ email }, function(err, user) {
        if (err) {
            console.error(err);
            res.status(500)
                .json({
                    error: 'Internal error please try again'
                });
        } else if (!user) {
            res.status(401)
                .json({
                    error: 'Incorrect email or password'
                });
        } else {
            user.isCorrectPassword(password, function(err, same) {
                if (err) {
                    res.status(500)
                        .json({
                            error: 'Internal error please try again'
                        });
                } else if (!same) {
                    res.status(401)
                        .json({
                            error: 'Incorrect email or password'
                        });
                } else {
                    // Issue token
                    const payload = { email };
                    const token = jwt.sign(payload, secret, {
                        expiresIn: '2m'
                    });
                    res.cookie('token', token, { httpOnly: true }).sendStatus(200);
                }
            });
        }
    });
};

const logout = function(req, res) {
    const token =
        req.body.token ||
        req.query.token ||
        req.headers['x-access-token'] ||
        req.cookies.token;

    const blackList = new BlackList({ token: token });
    blackList.save(function(err) {
        if (err) {
            res.status(500)
                .send("Error logout, please try again later.");
        } else {
            res.status(200).send("Logout confirm!");
        }
    });
};

const checkToken = function(req, res) {
    res.sendStatus(200).send('You have right token');
};

module.exports = {
    register,
    login,
    logout,
    checkToken
};