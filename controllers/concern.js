var express = require('express');
const jwt = require('jsonwebtoken');

const Concern = require('../models/concern.js');

const secret = 'likeme4';

const getConcern = function(req, res) {
    const { id } = req;
    if (id) {
        Concern.findOne({ _id: id }, { "_id": 1, "name": 1 },  { sort:{ "order":1 }}, function(err, concern) {
            concern.id = concern._id;
            delete concern._id;
            return res.status(200).send(concern);
        });
    } else {
        Concern.find({}, { "_id": 1, "name": 1 },  { sort:{ "order":1 }}, function(err, concerns) {
            if (err) {
                console.log(err);
                res.status(500).send("Error in working server.");
            }
            let result = {
                "isSuccess": true,
                "error": "string",
                "message": "string",
                "data": concerns.map(item => {
                	return{
                		id: item._id,
                		name: item.name
                	}
                })
            }
            return res.status(200).send(result);
        });
    }
};
const createNewConcern = function(req, res) {
    const { name } = req.query;
    if (name) {
        let query = Concern.findOne({}, {}, { sort:{ "order":-1 }}, function(err, lastConcern) {
            if (err) {
                console.log(err);
                res.status(500).send("Error in working server.");
            }
            if(lastConcern){
	            lastConcern = lastConcern.toObject();
            }
            let order = lastConcern && 
        			lastConcern.hasOwnProperty('order') && 
        			lastConcern.order || 0;
	        let concern = new Concern({ name: name, order: order + 1 });
	        concern.save(function(err, concern) {
	            if (err) {
	                console.log(err);
	                res.status(500).send("Error added concern");
	            } else {
	                res.status(200).send({
                		id: concern._id,
                		name: concern.name
                	});
	            }
	        })
        });
    } else {
        res.status(500).send("Error added concern");
    }
};

const updateConcern = function(req, res) {
    const { name } = req.query;
    const { id } = req.params;
    Concern.updateOne({ _id: id }, { name: name }, function(err, element) {
        if (err) {
            res.status(500).send("Error update concern" + err);
        } else {
            Concern.findOne({ _id: id }, { "_id": 1, "name": 1, "order": 1 }, function(err, concern) {
                if (err) {
                    res.status(500).send("Error update concern" + err);
                } else {
                    concern.id = concern._id;
                    delete concern._id;
                    let result = {
                        isSuccess: true, 
                        error: null, 
                        message: "", 
                        data: concern
                    }
                    res.status(200).send(result);
                }
            });
        	
        }
    })
};

const updateOrderConcern = function(req, res) {
    const data = req.body;
    console.log(data);
    let arrSending = [];
    let result = {
        data: null,
        error: null,
        isSuccess: true,
        message: "Operation Completed Successfully"
    }
    data.forEach((item) => {
        arrSending.push(new Promise((resolve, reject) => {
            Concern.updateOne({ _id: item.id }, { order: item.order }, function(err, element) {
                if (err) {
                    reject(err);
                }                        
                resolve(1);
            })
        }));
    })
    Promise.all(arrSending).then(values => {
        return res.status(200).send(result); 
    }, reason => {
        res.status(500).send(reason);
    });    
};

const deleteConcern = function(req, res) {
    const { id } = req.params;
    Concern.deleteOne({ _id: id }, function(err, element) {
        if (err) {
            res.status(500).send("Error delete concern" + err);
        } else {
            res.status(200).send({isSuccess:true});
        }
    })
};

module.exports = {
    getConcern,
    createNewConcern,
    updateConcern,
    deleteConcern,
    updateOrderConcern
};