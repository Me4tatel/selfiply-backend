const mongoose = require('mongoose');

const PatientSchema = new mongoose.Schema({
    personalNumber: { type: String, required: true },
    concernId: { type: String, required: false },
    patientSymptomsViewModel: { type: Array, required: false },
});

module.exports = mongoose.model('Patient', PatientSchema);