const mongoose = require('mongoose');

const ConcernSymptomSchema = new mongoose.Schema({
  idConcern: 	{ type: String, required: true },
  idSymptom: 	{ type: String, required: true },
  order: 		{ type: Number, required: true }
});

module.exports = mongoose.model('ConcernSymptom', ConcernSymptomSchema);