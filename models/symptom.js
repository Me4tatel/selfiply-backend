const mongoose = require('mongoose');

const SymptomSchema = new mongoose.Schema({
  inputType: { type: String, required: true },
  placeHolder: { type: String, required: false },
  text: { type: String, required: true },
});

module.exports = mongoose.model('Symptom', SymptomSchema);