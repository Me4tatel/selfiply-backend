const mongoose = require('mongoose');

const BlackListSchema = new mongoose.Schema({
  token: { type: String, required: true, unique: true },
});

module.exports = mongoose.model('BlackList', BlackListSchema);