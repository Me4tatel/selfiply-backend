const mongoose = require('mongoose');

const ConcernSchema = new mongoose.Schema({
  name: { type: String, required: true },
  order: { type: Number, required: true },
});

module.exports = mongoose.model('Concern', ConcernSchema);