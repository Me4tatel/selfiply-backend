const createError = require('http-errors');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

let indexRouter = require('./routes/index');
let authRouter = require('./routes/auth');
let concern = require('./routes/concern');
let concernSymptom = require('./routes/concernSymptom');
let patient = require('./routes/patient');
let symptom = require('./routes/symptom');

const mongo_uri = 'mongodb://localhost/riddarens';
const app = express();

app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({ type: ['json', '+json'] }));
app.use(cookieParser());

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('/api/Concerns', concern);
app.use('/api/ConcernSymptoms', concernSymptom);
app.use('/api/Patients', patient);
app.use('/api/Symptoms', symptom);
app.use('/api', authRouter);
app.use('/', indexRouter);


mongoose.connect(mongo_uri, { useNewUrlParser: true }, function(err) {
    if (err) {
        throw err;
    } else {
        console.log(`Successfully connected to ${mongo_uri}`);
    }
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});


const port = process.env.PORT || 62221;

app.listen(port, function() {
    console.log('Express server listening on port ' + port);
});